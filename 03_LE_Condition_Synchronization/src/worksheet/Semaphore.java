package worksheet;

public final class Semaphore {
	private int value;

	public Semaphore(int initial) {
		if (initial < 0) throw new IllegalArgumentException();
		value = initial;
	}
	
	private Object lock = new Object();

	public int available() {
		return value;
	}

	public void acquire() {
		synchronized (lock) {
			while (value == 0) {
				try {
					lock.wait();
				} catch (InterruptedException e) {}
			}
			value--;
		}
	}

	public void release() {
		synchronized(lock) {
			value++;
			lock.notify();
		}
	}
}
