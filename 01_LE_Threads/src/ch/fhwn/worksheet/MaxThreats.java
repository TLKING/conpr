package ch.fhwn.worksheet;

public class MaxThreats {

	public static void main(String[] args) {
		
		int n = 0;
		while (true) {
			System.out.println(n++);
			new Thread(() -> {
				try {
					Thread.sleep(Long.MAX_VALUE);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}).start();
		}
	}
}